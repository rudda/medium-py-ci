import unittest
import ../app/calculator

class TestCalculator(unittest.TestCase):

    #test add method
    def test_add_whenAParamIsNotNumber_thenReturnError(self):
        a, b= 'A',  2
        self.assertRaises(TypeError, calculator.add, a, b)

    # test sub method
    def test_sub_whenAIs5AndBis3_thenReturn2(self):
        a, b, expected =  5, 3, 2
        result = calculator.sub(a,b)
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
